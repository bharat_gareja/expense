import React from "react";
import "../assets/css/masterlayout.css"
import { Outlet } from "react-router-dom";
import Header from "../components/common/Header";

const MasterLayout: React.FC = () => {
  return (
    <div className="master-layout">
      <div className="header-component">
        <Header />
      </div>
      <div className="outlet-component"
      >
        <Outlet />
      </div>
    </div>
  );
};

export default MasterLayout;
