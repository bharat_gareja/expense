import React from "react";
import "./../assets/css/login.css";
import { Paper, Typography, Button ,Grid} from "@mui/material";
import { RootState } from "../redux/store";
import { login } from "../redux/features/userAuth/loginSlice";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { Link, useNavigate} from "react-router-dom"
import CustomTextField from "../components/common/CustomTextField";


interface FormValues {
  email: string;
  password: string;
}

const Login: React.FC = () => {
  
  const navigate = useNavigate()
  const dispatch = useDispatch();
  const users = useSelector((state: RootState) => state.userData.signUp);
  const loginSchema = Yup.object({
    email: Yup.string().required("Please enter an email"),
    password: Yup.string().required("Please enter a password"),
  });

  const handleFormSubmit = (values: FormValues): void => {
    const user = users.find(
      (user:any) => user.email === values.email && user.password === values.password
    );
    if (user) {
      const loginuser = {
        email: values.email,
        password: values.password,
      };
      dispatch(login(loginuser));
      navigate("/dashboard")
      localStorage.setItem("userData", JSON.stringify(loginuser));
    }
  };
  return (
    <>
    <div className="login-form">
    <Formik
        initialValues={{ email: "", password: "" }}
        validationSchema={loginSchema}
        onSubmit={handleFormSubmit}
        validate={(values) => {
            const errors: Partial<FormValues> = {};
            if (values.email) {
              const user = users.find((user:any) => user.email === values.email);
              if (!user) {
                errors.email = "User not found";
              } else if (user.password !== values.password) {
                errors.password = "Password does not match";
              }
            }
            return errors;
          }}
      >

        {({ values, handleBlur, handleChange, errors, touched }) => (
          <Paper elevation={10} className="paper">
            <Form>
              <div className="body-content">
                <Typography variant="h4" className="heading-text">
                  Log in
                </Typography>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Typography>Email address</Typography>
                    <CustomTextField
                    className="textfield"
                      type="email"
                      name="email"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      errorText={touched.email && errors.email}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Typography>Password</Typography>
                    <CustomTextField
                    className="textfield"
                      type="password"
                      name="password"
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      errorText={touched.password && errors.password}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <Button className="login-btn" type="submit">
                      Log in
                    </Button>
                  </Grid>
                </Grid>
                <div className="forget-text">
                <Typography>
                      Don't have an account? <Link to="/signup">Sign up</Link>
                    </Typography>
                </div>
              </div>
            </Form>
          </Paper>
        )}
      </Formik>
    </div>
  </>
  );
};

export default Login;
