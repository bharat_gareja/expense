import React, { useEffect, useState } from "react";
import "../assets/css/transaction.css"
import { RootState } from "../redux/store";
import { useSelector } from "react-redux";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import CustomTextField from "../components/common/CustomTextField";
import { Container , Typography} from "@mui/material";
interface TransactionDataItem {
  type: "income" | "expense";
  data: {
    income: string;
    date: string;
    categories: string;
    customNote: string;
  };
}
const Transaction: React.FC = () => {
  const [transactionData, setTransactionData] = useState<TransactionDataItem[]>(
    []
  );
  const [searchQuery, setSearchQuery] = useState("");
  const transaction = useSelector(
    (state: RootState) => state.transaction.transactions
  );
  useEffect(() => {
    setTransactionData(transaction);
  }, [transaction]);
  console.log(transaction);

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
      fontSize: 16,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 16,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));

  const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
  };
  const filteredTransactionData = transactionData.filter((item) =>
    item.data.categories?.toLowerCase().includes(searchQuery.toLowerCase())
  );
  return (
    <Container maxWidth='xl' className="table-container">
    
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 800 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell>TYPE</StyledTableCell>
              <StyledTableCell align="center">DATE</StyledTableCell>
              <StyledTableCell align="center">CATEGORY</StyledTableCell>
              <StyledTableCell align="center">CUSTOM NOTE</StyledTableCell>
              <StyledTableCell align="center">AMOUNT</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody className="centerText ">
            { transaction.length === 0 ? (
              <>
                <StyledTableRow>
                  <StyledTableCell className="h3404page" colSpan={5} align="center" >
                      Trasction  data doesn't exist
                  </StyledTableCell>
                </StyledTableRow>
              
          </>
       ) :(
            <>
            {transactionData.map((item: any, index: number) => (
              <StyledTableRow  key={index}>
                <StyledTableCell component="th" scope="row">
                  {item.type}
                </StyledTableCell>

                <StyledTableCell align="center">
                  {item.data.date}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {item.data.incomeType
                    ? item.data.incomeType
                    : item.data.categories}
                </StyledTableCell>

                <StyledTableCell align="center">
                  {item.data.customNote}
                </StyledTableCell>
                <StyledTableCell align="center">
                  {" "}
                  {item.data.amount ? (
                    <span style={{ color: "green" }}>+{item.data.amount}</span>
                  ) : (
                    <span style={{ color: "red" }}>-{item.data.expense}</span>
                  )}
                </StyledTableCell>
              </StyledTableRow>
            ))}
            </>
            ) 
            }
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};

export default Transaction;
