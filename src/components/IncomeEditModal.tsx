import React from "react";
import { useDispatch } from "react-redux";
import {
  Button,
  
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  
  Box 
} from "@mui/material";
import '../assets/css/incomeDetails.css'
import { useFormik } from "formik";
// import * as Yup from "yup";

import {
    Dialog,
    DialogContent,
    DialogTitle,
    DialogActions
    
  } from '@mui/material';
import {editIncome} from '../redux/features/income/incomeSlice'
import CustomTextField from "./common/CustomTextField";
import {
  AddCircleOutline as SalaryIcon,
  MonetizationOn as StockIcon,
  FitnessCenter as GymTrainerIcon,
  Work as FreelancingIcon,
  MoreHoriz as OthersIcon,
} from "@mui/icons-material";


// import CustomTextField from "./common/CustomTextFeild"


// const validationModalSchema = Yup.object().shape({
//     incomeType: Yup.string().required("Income type is required"),
//     customIncomeType: Yup.string().test('customTypeRequired', 'Custom Income Type is required when "Other" is selected', function(value) {
//       if (this.parent.incomeType === 'other') {
//         return !!value;
//       }
//       return true;
//     }),
//     amount: Yup.number().required("Amount is required").positive("Income must be a positive number"),
//     customNote: Yup.string(),
//     date: Yup.date().required("Date is required"),
//   });


  //props type define
  interface IncomeFormProps {
    open: boolean;
    onClose: () => void;
    incomeData: {
      incomeType: string;
      amount: string;
      date: string;
      customIncomeType?: string;
      customNote:string;
      index: number;

    };
    
  }

  

const IncomeEditModal: React.FC<IncomeFormProps> = ({ open, onClose, incomeData }) => {

  const incomeCategory:string[] = [
    "salary",
    "stock",
    "gymtrainer",
    "freelancing",
    "Others",
  ];
  const dispatch = useDispatch();
  
  const formik = useFormik({
    initialValues: {
      customIncomeType: incomeCategory.includes(incomeData?.incomeType) ? "" :incomeData?.incomeType,
      category: incomeCategory.includes(incomeData?.incomeType) ? incomeData?.incomeType : "Others",
      amount: incomeData.amount,
      customNote: incomeData.customNote,
      date: incomeData.date,
    },
    // validationSchema: validationModalSchema,
    onSubmit: (values:any) => {
        const updateData = {
            amount: values.amount,
            date: values.date,
            incomeType: values.category === "Others" ? values.customIncomeType : values.category, 
            customNote: values.customNote ? values.customNote : "- -", 
          };
          
      dispatch(editIncome({ index: incomeData.index, updatedData: updateData }));
    onClose();
    },
    
  });
  //handel close modal
  const handleClose = () => {
    onClose();
  };
  return (
    
    <Dialog open={open} onClose={onClose} fullWidth  disableEscapeKeyDown>
      <DialogTitle>Edit Income</DialogTitle>
      <DialogContent>

      <form onSubmit={formik.handleSubmit} >
      <Box className='incomeeditform'>
        <FormControl fullWidth margin="normal">
          <InputLabel id='category'>Income Type</InputLabel>
          <Select
            labelId="category"
            label="category"
            name="category"
            value={formik.values.category}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          >
            {incomeCategory.map((type) => (
             <MenuItem
             key={type}
             value={type}
             className="menu-item"
           >
             {type === "salary" && <SalaryIcon className="icon"  />}
             {type === "stock" && <StockIcon className="icon" />}
             {type === "gymtrainer" && <GymTrainerIcon className="icon" />}
             {type === "freelancing" && <FreelancingIcon className="icon" />}
             {type === "Others" && <OthersIcon className="icon" />}
             <span >{type}</span>
           </MenuItem>
            ))}
          </Select>
        </FormControl>
        
        {formik.values.category === "Others" && (
          <FormControl fullWidth margin="normal">
          <CustomTextField
            name="customIncomeType"
            label="Custom Income Type"
            type="text"
            value={formik.values.customIncomeType}
            onChange={formik.handleChange}
            errorText={
              formik.touched.customIncomeType &&
              Boolean(formik.errors.customIncomeType)
            }
            
          />
          </FormControl>
        )}
        <CustomTextField
          className="amount"
          name="amount"
          label="Amount"
          type="number"
          value={formik.values.amount}
          onChange={formik.handleChange}
          errorText={formik.touched.amount && Boolean(formik.errors.amount)}
        
          InputLabelProps={{
            shrink: true,
          }}
        />
        <CustomTextField
          name="customNote"
          label="Custom Note"
          value={formik.values.customNote}
          onChange={formik.handleChange}
          errorText={
            formik.touched.customNote && Boolean(formik.errors.customNote)
          }
          InputLabelProps={{
            shrink: true,
          }}
        />
        <CustomTextField
          name="date"
          label="Date"
          type="date"
          value={formik.values.date}
          onChange={formik.handleChange}
          errorText={formik.touched.date && Boolean(formik.errors.date)}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <DialogActions>
        <Button
          variant="contained"
          color="primary"
          type="submit"
        >
          Add
        </Button>
        <Button color="primary"  variant="contained"  onClick={handleClose}>Cancel</Button>
        </DialogActions>
        </Box>
      </form>
      </DialogContent>
      <DialogActions>
        
      </DialogActions>
    </Dialog>
 
  );

};

export default IncomeEditModal;