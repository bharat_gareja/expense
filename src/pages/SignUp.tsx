import React, { useState } from "react";
import logo from "./../assets/img/logo-square-65a6124237868b1d2ce2f5db2ab0b7c777e2348b797626816400534116ae22d7.svg";
import "../assets/css/signup.css";
import { Typography, Button } from "@mui/material";

import { Formik, Form } from "formik";
import * as Yup from "yup";
import { useNavigate , Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { signup } from "../redux/features/userAuth/signUpSlice";

import { RootState } from "../redux/store";
import CustomTextField from "../components/common/CustomTextField";

interface FormValues {
  name: string;
  email: string;
  password: string;
  confirmPassword: string;
}

const SignupSchema = Yup.object({
  name: Yup.string().required("Name is required"),
  email: Yup.string()
    .required("Email is required")
    .matches(
      /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/,
      "Invalid email format"
    ),
  password: Yup.string()
    .required("Password is required")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
      "Password must contain at least 8 characters, one uppercase letter, one lowercase letter, one number, and one special character"
    ),
  confirmPassword: Yup.string()
    .required("Confirm Password is required")
    .test("password-match", "Passwords must match", function (value) {
      return value === this.parent.password;
    }),
});

const Signup: React.FC = () => {
  



  const users = useSelector((state: RootState) => state.userData.signUp);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleFormSubmit = (values: FormValues) => {
    const error = users.find((user: any) => user.email === values.email);
    if (error) {
    }
    if (error == null) {
      dispatch(signup(values));
      const storedData = localStorage.getItem("signupData");
      const newData = storedData ? JSON.parse(storedData) : [];
      newData.push(values);
      localStorage.setItem("signupData", JSON.stringify(newData));
      navigate("/login");
    }
  };

  return (
    <>
      <div className="section">
        <div className="left-content">
          <img src={logo} alt="" className="logo" />
        </div>
        <div className="right-content">
          
          <Formik
            initialValues={{
              name: "",
              email: "",
              password: "",
              confirmPassword: "",
            }}
            validationSchema={SignupSchema}
            onSubmit={handleFormSubmit}
            validateOnChange={true}
            validate={(values) => {
              const errors: Partial<FormValues> = {};
              const error = users.find(
                (user: any) => user.email === values.email
              );
              if (error) {
                errors.email = "Email already exists";
              }
              return errors;
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              touched,
              errors,
            }) => (
              <Form className="form-data">
                <Typography className="starting-texts" variant="h5">
            {" "}
            Hi there! My name is
          </Typography>
                <CustomTextField
                  className="input-fields"
                  type="text"
                  label="Name"
                  name="name"
                  value={values.name}
                  onBlur={handleBlur}
                  errorText={touched.name && errors.name}
                  onChange={(e) => {
                    handleChange(e);
                    
                  }}
                ></CustomTextField>

                
                  <>
                    <Typography
                      className="starting-texts"
                      variant="h6"
                    >
                      {" "}
                      Here’s my{" "}
                      <span style={{ fontWeight: "bolder" }}>
                        email address:
                      </span>
                    </Typography>
                    <CustomTextField
                      className="input-fields"
                      type="email"
                      label="Email"
                      name="email"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      errorText={touched.email && errors.email}
                    />

                    <Typography
                      className="starting-texts"
                      variant="h6"
                    >
                      {" "}
                      And here’s my{" "}
                      <span style={{ fontWeight: "bolder" }}>Password:</span>
                    </Typography>
                    <CustomTextField
                      className="input-fields "
                      type="password"
                      label="Password"
                      name="password"
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      errorText={touched.password && errors.password}
                    />

                    <Typography
                      className="starting-texts"
                      variant="h6"
                    >
                      {" "}
                      And my{" "}
                      <span style={{ fontWeight: "bolder" }}>
                        Confirm password:
                      </span>
                    </Typography>
                    <CustomTextField
                      className="input-fields"
                      type="password"
                      label="Confirm Password"
                      name="confirmPassword"
                      value={values.confirmPassword}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      errorText={
                        touched.confirmPassword && errors.confirmPassword
                      }
                    />
                  </>
               
                <div className="btn">
                  <Button type="submit" className="signup-btn">
                    Sign me up!
                  </Button>
                </div>
                <div >
                  <Typography className="sign-up-link">
                    Already have account ?
                  <Link to="/login">login</Link>
                  </Typography>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </>
  );
};

export default Signup;
