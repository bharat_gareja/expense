import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Grid, Paper, Typography, Container } from "@mui/material";
import { showIncomeForm } from "../redux/features/income/incomeSlice";
import Income from "./Income";
import ExpenseBarCharts from "../components/charts/ExpenseBarCharts";
import { ExpenseDonutChart } from "../components/charts/ExpenseDonutChart";
import { RootState } from "../redux/store";
import Expenses from "../components/Expenses";
import { showExpenseForm } from "../redux/features/expense/expenseSlice";
import "../assets/css/dashboard.css";


const Dashboard: React.FC = () => {


  const totalIncome = useSelector(
    (state: RootState) => state.income.totalIncome
  );
  const totalExpense = useSelector(
    (state: RootState) => state.expense.totalExpense
  );
  const dispatch = useDispatch();

  return (
    <Container maxWidth="xl" className="dashboard-conatiner">
      <div className="expense-bar-chart-container">
        <div className="dashboard-header">
          {" "}
          <Typography className="dashboard-title">Dashboard</Typography>{" "}
        </div>
        <div className="header-container">
          <Grid container justifyContent="space-between" alignItems="center">
            <Grid item xs={12} md={6} marginBottom="10px">
              <div className="totals-container">
                <div className="total-amount-container">
                  <Typography className="total-amount-label">
                    Total Amount
                  </Typography>
                  <Typography className="total-amount-value">
                    {totalIncome}
                  </Typography>
                </div>
                <div className="total-expense-container">
                  <Typography className="total-expense-label">
                    Total Expense
                  </Typography>
                  <Typography className="total-expense-value">
                    {totalExpense}
                  </Typography>
                </div>
              </div>
            </Grid>
            <Grid item>
              <div className="button-container">
                <Button
                  onClick={() => dispatch(showIncomeForm(true))}
                  variant="contained"
                  color="primary"
                  className="custom-button"
                >
                  Add Income
                </Button>
                <Button
                  onClick={() => dispatch(showExpenseForm(true))}
                  variant="contained"
                  color="secondary"
                  className="custom-button"
                >
                  Add Expense
                </Button>
              </div>
            </Grid>
          </Grid>
        </div>

        <Grid container spacing={3}>
          <Grid item xs={12} md={6}>
            <Income />
          </Grid>
          <Grid item xs={12} md={6}>
            <Expenses />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className="custom-paper">
              <ExpenseBarCharts />
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className="custom-paper">
              <ExpenseDonutChart />
            </Paper>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Dashboard;
