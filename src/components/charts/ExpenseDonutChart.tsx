import React from 'react';
import { useSelector } from 'react-redux';
import ReactApexChart from 'react-apexcharts';
import { RootState } from '../../redux/store';
import { ApexOptions } from 'apexcharts';

export const ExpenseDonutChart: React.FC = () => {
  const expenses = useSelector((state: RootState) => state.expense.expense);
  console.log(expenses);
  
  const income = useSelector((state: RootState) => state.income.totalIncome);
 
  const donutChartData: { x: string; y: number }[] = [];

  expenses.forEach((expense: any) => {
    const category = expense.categories;
    const expenseAmount = expense.expense;
    const existingCategoryIndex = donutChartData.findIndex((data) => data.x === category);

    if (existingCategoryIndex === -1) {
      const percentage = (expenseAmount / income) * 100;
      donutChartData.push({ x: category, y: percentage });
    }
  });

  const chartOptions = {
    chart: {
      id: 'expense-donut-chart',
      toolbar: {
        show: false,
      },
    },
    labels: donutChartData.map((data: any) => data.x),
    dataLabels: {
      enabled: true,
      formatter: (val: any) => `${val.toFixed(2)}%`,
    },
    legend: {
      show: true,
      position: 'right',
    },
    plotOptions: {
      pie: {
        size: '70%',
        donut: {
          size: '50%',
        },
      },
    },
  } as ApexOptions;

  return (
    <div>
      <h3>Expense Categories (Donut Chart)</h3>
      <ReactApexChart type="donut" options={chartOptions} series={donutChartData.map((data: any) => data.y)} width="100%" height={350}/>
    </div>
  );
};
